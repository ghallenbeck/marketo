# De-Dupe Algorithm

* Input list is in ascending date order
    * Can iterate in reverse to check most recent dates first. This will grab the newest dupe, and
    * will grab the last entry for dupes with identical date strings.
* Store seen entries in a "hashmap" of IDs and email addresses.
    * If the neither the ID nor the email are in the hash table, the current entry is not a dupe.

## Filter vs. Iteration

This can be done in-place using the JS built-in `Array.prototype.filter`. This will require
reversing the array first, and again after the array has been filtered.
* Time: O(3n)

Another method would be to create a new array for the de-deduped list. Push the first-seen of each
entry into the list, and store its unique key. We could use the queue-like methods for Arrays to
avoid having to reverse the list, but they average O(n) time complexity, so they are not the best
option. To optimize the time complexity, we can push the items onto the array and then reverse it
when we are done.
* Time: O(2n)

Both of these methods accomplish the same thing, which is to create a new array with the
appropriate entries. By iterating manually, we can control the iteration direction meaning a slight
optimization in time complexity.

In addition to time complexity, `Array.prototype.filter` hides the algorithm behind a built-in
function, so it might be less readable for developers with little experience using the function.