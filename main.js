const fs = require('fs');
const path = require('path');

const FILE_PATH = './leads.json';
const LOG_PATH = './changelog';
const OBJ_PROP = 'leads';
const OUTPUT_PATH = path.basename(FILE_PATH, path.extname(FILE_PATH)) + '_deduped.json';


fs.readFile(FILE_PATH, 'utf8', function (err, data) {
    // We use a writable stream for the log file so that we can write directly to the file at any
    // point in the script.
    var log = fs.createWriteStream(LOG_PATH),
        obj = null;

    if(err) {
        console.error(err);
        return;
    }
    else {
        log.write('Deduplication of file: ' + FILE_PATH + '\n\n');

        // File contents are read as a string, so we will attempt to parse it as JSON so we can work
        // with it. If the file is not properly formatted JSON, we will exit with an error.
        try {
            obj = JSON.parse(data);
            log.write('Original File:\n' +
                      JSON.stringify(obj, null, 4) + '\n\n');
        }
        catch (err) {
            console.error(err);

            log.end('ERROR:\n' +
                    err);

            return;
        }


        obj[OBJ_PROP] = deDupe(obj[OBJ_PROP], log);

        fs.writeFile(OUTPUT_PATH, JSON.stringify(obj, null, 4), function (err) {
            if(err) {
                console.error(err);
                log.end('\n\nERROR:\n' +
                        err);
            }
            else {
                console.log('SUCCESS: Output saved to: ' + OUTPUT_PATH + '\n' +
                            '         Logfile saved to: ' + LOG_PATH);
                log.end('\n\nDeduplication Finished\n' +
                        JSON.stringify(obj, null, 4));
            }
        });
    }
});


/**
 * Removes duplicates from the given array.
 *
 * @param {Object|Array} arr - The array to de-dupe.
 * @param {WriteSteam} log - A writable stream where changes can be logged.
 *
 * @return {Object|Array} A new array without duplicates.
 */
function deDupe(arr, log) {
    var deduped = [];
    var seen = { id: {}, email: {} };  // Use an object as a hashmap to remember seen entries.

    // Iterate over the array in reverse to give preference to most recent entries.
    for(let i = arr.length - 1; i >= 0; i--) {
        // An item is considered a duplicate if either the ID or email is identical to another item.
        // For this to be a unique entry, neither its ID nor email should have been seen already.
        if(seen.id[arr[i]['_id']] === undefined && seen.email[arr[i].email] === undefined) {
            seen.id[arr[i]['_id']] = arr[i]['_id'];
            seen.email[arr[i].email] = arr[i].email;

            deduped.push(arr[i]);
        }
        else {
            log.write('Removed Duplicate Record:\n' + JSON.stringify(arr[i], null, 4) + '\n');
        }
    }

    // Since we iterated in reverse, the deduped array is in reverse order and needs to be
    // corrected.
    return deduped.reverse();
}