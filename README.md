# De-Dupe Set in `leads.json`

1. Input data will be in date order
2. The data from the newest date should be preferred
3. Duplicate IDs count as dupes, duplicate emails count as dupes. Both must be unique in the dataset. Duplicate values elsewhere do not count as dupes.
4. If the dates are identical, the data from the record provided last in the list should be preferred.
